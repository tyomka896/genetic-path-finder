import { Path } from './path'
import { mainPath, valueElem } from './variables'

const INTERVAL = 0

let lastDist = 0
let mainBreed = []
let timeout = setTimeout(mainAlgorithm, INTERVAL)

function mainAlgorithm() {
    if (mainPath.length < 3) {
        timeout = setTimeout(mainAlgorithm, INTERVAL)
        return
    }

    if (!mainBreed[0] || mainBreed[0].length !== mainPath.length) {
        mainBreed = [
            new Path({ dist: mainPath.dist, points: [...mainPath.points] }),
        ]
    }

    crossing(500)
    elimination(1)

    mainPath.points = [...mainBreed[0].points]
    mainPath.evalDist()

    if (lastDist !== mainPath.dist) {
        lastDist = mainPath.dist
        valueElem.textContent = lastDist.toFixed(2)
    }

    timeout = setTimeout(mainAlgorithm, INTERVAL)
}

function crossing(count) {
    let standard = mainBreed[0]

    while (mainBreed.length < count) {
        const parent = [...standard.points]

        const timesToChange = getRandom(parent.length < 5 ? parent.length : 5)

        for (let l = 0; l < timesToChange; l++) {
            const left = getRandom(parent.length - 1)
            const right = getRandom(parent.length - 1)

            let _temp = parent[left]
            parent[left] = parent[right]
            parent[right] = _temp
        }

        const child = new Path({ points: parent })
        child.evalDist()

        if (standard.dist > child.dist) {
            standard = child
        }

        mainBreed.push(child)
    }

    return
}

function elimination(count) {
    mainBreed.sort((a, b) => a.dist - b.dist)

    mainBreed.splice(count)
}

function getRandom(min, max) {
    if (!max && min === 0) return 0

    if (!max) {
        max = min
        min = 0
    }

    return Math.floor(Math.random() * (max - min + 1) + min)
}
