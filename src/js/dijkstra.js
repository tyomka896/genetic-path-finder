import { mainPath, valueElem } from './variables'

const INTERVAL = 100

let lastLen = 0
let timeout = setTimeout(mainAlgorithm, INTERVAL)

function mainAlgorithm() {
    if (mainPath.length < 3 || lastLen === mainPath.length) {
        timeout = setTimeout(mainAlgorithm, INTERVAL)
        return
    }

    console.log('Dijkstra. . .')
    dijkstraAlgorithm()

    valueElem.textContent = mainPath.dist.toFixed(2)
    lastLen = mainPath.length

    timeout = setTimeout(mainAlgorithm, INTERVAL)
}

// Simple Dijksta implementation,
// the algorithm tries to find next minimal distance
// from current point to any point nearby
function dijkstraAlgorithm() {
    const points = [...mainPath.points]

    let final = Infinity
    let best = new Set()

    for (const pt of points) {
        let nextPt = pt
        let currentFinal = 0

        const ptVisited = new Set([pt])

        while (ptVisited.size < points.length) {
            let minPt = undefined

            for (const ptNext of points) {
                if (ptVisited.has(ptNext)) {
                    continue
                }

                if (minPt === undefined) {
                    minPt = ptNext

                    continue
                }

                // const prevValue = paths[pathKey(nextPt, minPt)]
                const prevValue = nextPt.dist(minPt)
                // const nextValue = paths[pathKey(nextPt, ptNext)]
                const nextValue = nextPt.dist(ptNext)

                if (prevValue > nextValue) {
                    minPt = ptNext
                }
            }

            if (minPt !== undefined) {
                ptVisited.add(minPt)

                currentFinal += nextPt.dist(minPt)
                nextPt = minPt
            }
        }

        if (currentFinal < final) {
            final = currentFinal
            best = ptVisited
        }
    }

    mainPath.points = Array.from(best)
    mainPath.evalDist()
}
