import { defineConfig } from 'vite'
import { babel } from '@rollup/plugin-babel'

export default defineConfig(({ command }) => {
    if (command === 'serve') return {}

    return {
        plugins: [
            babel({ babelHelpers: 'bundled', presets: [ '@babel/preset-env' ] }),
        ],
        build: {
            outDir: 'public',
            emptyOutDir: true,
            rollupOptions: {
                output: {
                    entryFileNames: 'js/[name]-[hash].js',
                    chunkFileNames: '_/[name]-[hash].js',
                    assetFileNames: ({ name }) => {
                        if (/\.(gif|jpe?g|png|svg)$/.test(name ?? ''))
                            return 'img/[name]-[hash][extname]'
                        else if (/\.css$/.test(name ?? ''))
                            return 'css/[name]-[hash][extname]'
                        else if (/\.ico$/.test(name ?? ''))
                            return '[name]-[hash][extname]'
                        else return 'assets/[name]-[hash][extname]'
                    },
                }
            }
        },
    }
})
