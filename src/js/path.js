/**
 * Current displayed path
 */
export class Path {
    #dist = 0;

    constructor(options) {
        this.#dist = options?.dist || 0
        this.points = options?.points || []
    }

    get dist() {
        return this.#dist
    }

    get length() {
        return this.points.length
    }

    addPoint(point) {
        this.points.push(point)
        this.evalDist()
    }

    evalDist() {
        const count = this.points.length

        if (count <= 1) {
            return 0
        }

        let _dist = 0

        for (let l = 0; l < count - 1; l++) {
            _dist += this.points[l]
                .dist(this.points[l + 1])
        }

        _dist += this.points[0]
            .dist(this.points[count - 1])

        this.#dist = _dist
    }
}
