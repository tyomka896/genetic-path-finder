import { mainPath } from './variables'

function setup(p) {
    p.createCanvas(600, 300)
}

function draw(p) {
    p.background(255)

    const points = mainPath.points

    p.noFill()
    p.stroke('#000')
    p.strokeWeight(1)
    p.beginShape()
    if (points.length) p.curveVertex(points[0].x, points[0].y)
    for (let point of points) {
        p.curveVertex(point.x, point.y)
    }
    if (points.length) p.curveVertex(points[0].x, points[0].y)
    if (points.length > 1) p.curveVertex(points[1].x, points[1].y)
    p.endShape()

    p.fill('#333')
    p.noStroke()
    for (let point of points) {
        p.ellipse(point.x, point.y, 15, 15)
    }
}

function mouseClicked(p) {
    const point = p.createVector(p.mouseX, p.mouseY)

    if (point.x < 5 || point.x > p.width - 5 ||
        point.y < 5 || point.y > p.height - 5) return

    mainPath.addPoint(point)
}

new p5((p) => {
    p.setup = () => setup(p)
    p.draw = () => draw(p)
    p.mouseClicked = () => mouseClicked(p)
}, 'canvas')
